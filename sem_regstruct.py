from sem_opt_classic import SEMOptClassic as ClassicalOptimiser
#from .validation import separate_csv
from sem_model import SEMData, SEMModel
from sem_stats import calculate_p_values
from sem_stats import calculate_aic, calculate_bic, calculate_likelihood
from copy import deepcopy
from collections import namedtuple, defaultdict
import numpy as np

RStepResult = namedtuple('RStepResult', ['f_ml', 'f_regu', 'ml', 'aic', 'bic',
                                         'params', 'cv_mls'])
RegsemResult = namedtuple('RegsemResult', ['ml', 'aic', 'bic', 'params'])

def move_params(modelA: SEMModel, modelB: SEMModel, optB: ClassicalOptimiser):
    modelA.params = np.zeros_like(optB.params)
    for i, (mx, pos1, pos2) in modelA.param_pos.items():
        if mx == 'Psi' and pos1 != pos2:
            continue
        v1, v2 = modelA.get_vars_from_pos(mx, pos1, pos2)
        k = modelB.get_param_pos_from_vars(mx, v1, v2)
        modelA.params[i] = optB.params[k]

def separate_data(mod: SEMModel, filename: str, k):
    if k == 1:
        d = SEMData(mod, filename)
        return [d, d]
    return [SEMData(mod, filename, interval=(i / k, (i + 1) / k)) for i in range(k)]

def get_desc_from_beta_params(refModel: SEMModel, params: list):
    def sub_translate(d, op, varsAdded):
        desc = str()
        for y, xs in dDescriptions.items():
            s = str()
            varsAdded.add(y)
            s = '{} {} '.format(y, op)
            it = iter(xs)
            x = next(it)
            varsAdded.add(x)
            s += x
            for x in it:
                s += ' + {}'.format(x)
                varsAdded.add(x)
            desc += s + '\n'
        return desc
    SPart = refModel.d_vars['SPart']
    MPart = refModel.d_vars['MPart']
    dDescriptions = defaultdict(list)
    varsAdded = set()
    for i, j in params:
        y, x = SPart[i], SPart[j]
        dDescriptions[y].append(x)
    
    description = sub_translate(dDescriptions, '~', varsAdded)
    if not varsAdded:
        return None
    dDescriptions = defaultdict(list)
    m = refModel.matrices['Lambda']
    t = [(i, j) for i, j in map(list, zip(*(np.where(np.isclose(m, 1)))))
         if MPart[i] in refModel.d_vars['Manifest']]
    for i, j in t:
        eta = SPart[j]
        ind = MPart[i]
        dDescriptions[eta].append(ind)
    for i, (mx, k, m) in refModel.param_pos.items():
        if mx == 'Lambda':
            eta = SPart[m]
            ind = MPart[k]
            dDescriptions[eta].append(ind)
    description += sub_translate(dDescriptions, '=~', varsAdded)
    for leftOver in (set(SPart) | set(MPart)) - varsAdded - set(refModel.d_vars['Lat']):
        description += '{} ~~ {}\n'.format(leftOver, leftOver)
    return description

def get_full_model(refModel: SEMModel):
    indices = [ind for ind in
               np.ndindex(refModel.matrices['Beta'].shape)
               if ind[0] != ind[1]
               ]
              # np.nditer(np.tril_indices_from(refModel.matrices['Beta'], -1))]
    return SEMModel(get_desc_from_beta_params(refModel, indices), False)

def get_meaningful_parameters(refModel: SEMModel, pvalues, pvalue_cutoff=0.01):
    ret = list()
    for i, pvalue in enumerate(pvalues):
        if pvalue < pvalue_cutoff and refModel.param_pos[i][0] == 'Beta':
            ret.append(refModel.param_pos[i][1:])
    return ret

def check_model(model: SEMModel, opt: ClassicalOptimiser, data: SEMData):
    inds = [i for i, (mx, j, k) in model.param_pos.items() if mx == 'Beta']
    pvals = np.array(calculate_p_values(opt, data))
    prct = np.count_nonzero(pvals[inds] < 0.5) / len(inds)
    if np.any(np.isnan(pvals)) or prct < 0.6:
        return False
    return True

def regsem_step(fullModel: SEMModel, paramsToPen, betaParams, fullOpt: ClassicalOptimiser,
                fullData: SEMData, validationSets, a, regu, tmpBuffer,
                prevMl=None):
    if prevMl is None or np.isnan(prevMl) or prevMl > 1e6:
        fullOpt.reset_params()
    fMl, fRegu = fullOpt.optimize('MLW', regularization=regu,
                                  paramsToPenalize=paramsToPen, alpha=a)
    print('Step:', a, fMl, fRegu)
    paramsToPen = np.array(paramsToPen)
    betaParams = np.array(betaParams)
    pvals = np.array(calculate_p_values(fullOpt, fullData))
    meaningfulParams = tuple(get_meaningful_parameters(fullModel, pvals))
    if meaningfulParams in tmpBuffer:
        t = tmpBuffer[meaningfulParams]
        if t is not None:
            t = RStepResult(fMl, fRegu, t.ml, t.aic, t.bic, t.params, t.cv_mls)
        return t, (fullOpt.params[betaParams], pvals[betaParams])

    desc = get_desc_from_beta_params(fullModel, meaningfulParams)
    if desc is None:
        tmpBuffer[meaningfulParams] = None
        return None, (None, None)
    aic, bic, ml = 0, 0, 0
    model = SEMModel(desc, False)
    mls = list()
    for data in validationSets:
      #  data = SEMData(model, csvData)
        model.load_initial_dataset(data)
     #   move_params(model, fullModel, fullOpt)
        opt = ClassicalOptimiser(model, data)
        tml, _ = opt.optimize('MLW')
        if np.isnan(tml) or abs(tml) > 1e4 or not check_model(model, opt, data):
            if np.isnan(tml) or abs(tml) > 1e4:
                mls.append(np.nan)
            else:
                mls.append(tml)
            i = len(validationSets) - len(mls)
            for j in range(i): mls.append(np.nan)
            t = RStepResult(fMl, fRegu, np.nan, np.nan, np.nan, desc, mls)
            tmpBuffer[meaningfulParams] = t
            return t, (fullOpt.params[betaParams], pvals[betaParams])
        aic += calculate_aic(opt, data, lh=tml)
        bic += calculate_bic(opt, data, lh=tml)            
        mls.append(tml)
    k = len(validationSets)
    aic /= k; bic /= k; ml = np.mean(mls)
    r = RStepResult(fMl, fRegu, ml, aic, bic, desc, mls)
    tmpBuffer[meaningfulParams] = r
    return r, (fullOpt.params[betaParams], pvals[betaParams])


def test_step(fullModel: SEMModel, paramsToPen, betaParams, fullOpt, a, regu):
  #  fullOpt.reset_params()
    fMl, fRegu = fullOpt.optimize('MLW', regularization=regu,
                                  paramsToPenalize=paramsToPen, alpha=a)
    print('{:.3f} {:.3f} {:.3f} {:.3f}'.format(a, fMl, np.sqrt(fRegu), np.linalg.norm(fullOpt.params[paramsToPen])))
    paramsToPen = np.array(paramsToPen)
    betaParams = np.array(betaParams)
    return fullOpt.params[betaParams]

def test(model, csvData, a=0, b=50, step=1, regu='l2'):
    fullModel = get_full_model(model)
   # trainingSet, validationSets = separate_csv(csvData, 4)
    data = SEMData(fullModel, csvData)
    fullModel.load_initial_dataset(data)
    fullOpt = ClassicalOptimiser(fullModel, data)
    paramsToPenalize = fullModel.get_params_from_matrices(['Beta'])
    betaParams = fullModel.get_params_from_matrices(['Beta'])
    lambdas = np.arange(a, b, step)
    values = []
    norms = []
  #  fullOpt.optimise('MLW')
   # test_step(fullModel, paramsToPenalize, betaParams, fullOpt,
   #                               0.075, regu)
   # fullOpt.reset_params
    for l in lambdas:
        val = test_step(fullModel, paramsToPenalize, betaParams, fullOpt,
                        l, regu)
        values.append(val)
        norms.append(np.linalg.norm(val))
    return lambdas, values, norms

def regsem(model, csvData, a=0, b=80, step=0.1, regu='l2', flexibleAlpha=False):
    fullModel = get_full_model(model)
    datas = separate_data(fullModel, csvData, 1)
    trainingSet, validationSets = datas[0], datas[1:]
    fullModel.load_initial_dataset(trainingSet)
    fullOpt = ClassicalOptimiser(fullModel, trainingSet)
    paramsToPenalize = fullModel.get_params_from_matrices(['Beta'])
    betaParams = fullModel.get_params_from_matrices(['Beta'])
    if flexibleAlpha:
        t = fullOpt.get_regularization(regu, paramsToPenalize)(fullOpt.params)
        print(t, a, b)
        a /= t; b /= t; step /= t
        print(a,b,step)
    lambdas = np.arange(a, b, step)
    tmpBuffer, tmpResults = dict(), list()
    lmbds, params, pvals = list(), list(), list()
    prevMl = None
    for l in lambdas:
        fullOpt.optimize('MLW')
        r, (fo, pv) = regsem_step(fullModel, paramsToPenalize, betaParams, fullOpt, trainingSet,
                                  validationSets, l, regu, tmpBuffer, prevMl)
        if not isinstance(r, type(None)):# and not np.isnan(r.ml):
            prevMl = r.f_ml
            tmpResults.append(r)
            print('{:.3f} {:.3f} {:.3f} {:.3f}'.format(l, r.ml, r.bic, np.linalg.norm(fo)))
           # print(r.params)
            params.append(fo)
            pvals.append(pv)
            lmbds.append(l)
        #print(l, r.f_ml, r.ml)
    try:
        t = [r for r in tmpResults if not np.isnan(r.ml)]
        mlBest = min(t, key=lambda r: r.ml)
        aicBest = min(t, key=lambda r: r.aic)
        bicBest = min(t, key=lambda r: r.bic)
    except ValueError:
        mlBest, aicBest, bicBest = None, None, None
    return (mlBest, aicBest, bicBest), (lmbds, tmpResults, params, pvals)