from sem_model import SEMModel
import graphviz as gv
import numpy as np

def visualize(model: SEMModel, out=None, values=None):
    g = gv.Digraph(format='jpg')
    dSPart = {v: k for k, v in model.d_vars['D_SPart'].items()}
    dMPart = {v: k for k, v in model.d_vars['D_MPart'].items()}
    #BETA
    g.node_attr.update(color='red', shape='box')
    params = [(k, m) for i, (mx, k, m) in model.param_pos.items()
              if mx == 'Beta']
    mx = model.matrices['Beta']
   # t = list(map(list, zip(*np.where(np.abs(mx) >= 1e-16))))
   # params.extend(ind for ind in t if ind not in params)
    for i, j in params:
            g.edge(dSPart[j], dSPart[i])
    #LAMBDA
    params = [(k, m) for i, (mx, k, m) in model.param_pos.items()
              if mx == 'Lambda']
    mx = model.matrices['Lambda']
    t = list(map(list, zip(*np.where(np.isclose(mx, 1)))))
    params.extend(ind for ind in t if ind not in params and dMPart[ind[0]]
                  in  model.d_vars['Manifest'])
    for i, j in params:
            g.node(dSPart[j], shape='circle', color='red')
            g.node(dMPart[i], shape='box', color='black')
            g.edge(dSPart[j], dMPart[i])

    g.render(out, view=True if out is None else False)
        
