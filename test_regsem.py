from sem_model import SEMModel, SEMData
from sem_opt_classic import SEMOptClassic
from sem_opt_bayes import SEMOptBayes
from sem_inspector import inspect, inspect_mx
from sem_visualization import visualize
from sem_opt_phylo import SEMModelNode
from sem_stats import gather_statistics
import matplotlib.pyplot as plt
from sem_regstruct import test, regsem
import numpy as np
from scipy.optimize import minimize


estimator = 'MLW'



# path_pref = 'schiza
# file_model = path_pref + 'mod_akt_single_cut.txt'
# file_data = path_pref + 'exprs_control.txt'

path_model = 'testingData/models/'
path_data = 'testingData/models/'
path_res = 'res/'
file_model = 'mod9.txt'
file_data = 'data9.txt'

#
# path_model = 'phylogeny/'
# path_data = 'phylogeny/'
# file_data = 'NEU.txt'
# file_model = 'mod_blood.txt'

mod = SEMModel(path_model + file_model)
#visualize(mod)
data = SEMData(mod, path_data + file_data)
mod.load_initial_dataset(data)
opt = SEMOptClassic(mod, data)

for i in range(1000):
    opt.ml_wishart_gradient(opt.params)


#res, _ = opt.optimize('MLW', opt_method='SLSQP')
#s = gather_statistics(opt, data)
