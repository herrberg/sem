from sem_model import SEMData, SEMModel
import numpy as np
from scipy.optimize import minimize, line_search
from sem_opt_abc import SEMOptABC
import portmin as pm

class SEMOptClassic(SEMOptABC):

    def __init__(self, mod: SEMModel, data: SEMData):
        """

        :param mod:
        :param data:
        :param estimator:
        :param regularizator:
        """

        super().__init__(mod, data)
        # Loss-functional and its additional parameter



    @staticmethod
    def loss_functions() -> dict:
        """
        Create the dictionary of possible functions
        :return:
        """
        tmp_dict = dict()
        tmp_dict['ULS'] = SEMOptClassic.unweighted_least_squares
        tmp_dict['GLS'] = SEMOptClassic.general_least_squares
        tmp_dict['WLS'] = SEMOptClassic.general_least_squares
        tmp_dict['MLW'] = SEMOptClassic.ml_wishart
        tmp_dict['MLN'] = SEMOptClassic.ml_normal
        tmp_dict['Naive'] = SEMOptClassic.naive_loss
        return tmp_dict
        
    def get_obj_function(self,  name: str):
        objDict = {'ULS': self.unweighted_least_squares,
                   'MLW': self.ml_wishart,
                   'MLN': self.ml_normal,
                   'GLS': self.general_least_squares}
        try:
            return objDict[name]
        except KeyError:
            raise Exception("Unknown optimization method {}.".format(name))

    def get_gradient_function(self, name: str):
        gradDict = {'MLW': self.ml_wishart_gradient,
#                    'ULS': self.uls_gradient,
#                    'GLS': self.gls_gradient,
                    'l1':  self.regu_l1_gradient,
                    'l2':  self.regu_l2_gradient}
        if name in gradDict:
            return gradDict[name]
        else:
            return None

    def get_regularization(self, name: str, paramsToPenalize:list):
        reguDict = {'l1': self.regu_l1,
                    'l2': self.regu_l2}
        try:
            f = reguDict[name]
            if paramsToPenalize is None or len(paramsToPenalize) == 0:
                return f
            else:
                def regu_wrapper(params):
                    params = params[regu_wrapper.paramsToPenalize]
                    return regu_wrapper.f(params)
                regu_wrapper.f = reguDict[name]
                regu_wrapper.paramsToPenalize = paramsToPenalize
                return regu_wrapper
        except KeyError:
            raise Exception("Unknown regularization method {}.".format(name))

    def compose_loss_function(self, method: str, regularization=None,
                              paramsToPenalize=None, a=1.0):
        """Build a loss function.
        Key arguments:
        method -- a name of an optimization technique to apply.
        regularization -- a name of regularizatio technique to apply.
        paramsToPenalize -- indicies of parameters from params' vector to 
                            penalize. If None, then all params are penalized.
        a - a regularization multiplier.
        Returns:
        (Loss function, obj_func, a * regularization)
        Loss function = obj_func + a * regularization"""
        obj_func = self.get_obj_function(method)
        regu = None
        if regularization is not None:
            regu = self.get_regularization(regularization, paramsToPenalize)
            loss_func = lambda params: obj_func(params) + a * regu(params)
        else:
            loss_func = lambda params: obj_func(params)
        grad = self.compose_gradient_function(method, regularization, a,
                                              paramsToPenalize)
        return (loss_func, obj_func, regu, grad)

    def compose_gradient_function(self, method: str, regu=None, a=1.0,
                                  paramsToPen=None):
        """ Builds a gradient function if possible. """
        grad = None
        grad_of = self.get_gradient_function(method)
        if grad_of is not None:
            grad = grad_of
        if regu is not None and grad is not None:
            regu = self.get_gradient_function(regu)
            if regu is None:
                return None
            def grad_composed(params):
                g = grad_of(params)
                rg = a * regu(params)
                mask = np.ones(len(params), np.bool)
                mask[paramsToPen] = False
                rg[mask] = 0
                return g + rg
            grad = grad_composed
        return grad

    def optimize(self, method, regularization=None,
                 alpha=1.0, paramsToPenalize=None, opt_method='Adam', **args):
        """

        :param optMethod:
        :param bounds:
        :param alpha:
        :return:
        """
        scipy_methods = ('SLSQP', 'dogleg')
        stochastic_methods = {'Adam': self.minimize_adam,
                              'Momentum': self.minimize_momentum,
                              'Nesterov': self.minimize_nesterov}

        params_init = self.params

        # Minimisation
        options = {'maxiter': 1e3, 'ftol': 1e-8}
        lf, of, regu, grad = self.compose_loss_function(method,
                                                        regularization,
                                                        paramsToPenalize,
                                                        alpha)
        if grad is None:
            print("Warning: analytical gradient is not available.")
        #grad = None
       # lf = func_to_min
      #  cons = ({'type': 'ineq', 'fun': lambda p: self.constraint_all(p)})
        if opt_method in scipy_methods:
            res = minimize(lf, params_init, jac=grad, hess=self.ml_wishart_hessian,
                           bounds=self.param_bounds, method=opt_method, options=options)
            self.params = np.array(res.x)
        elif opt_method in stochastic_methods:
            tmp_cov = self.m_cov.copy()
            method = stochastic_methods[opt_method]
            self.params = method(grad, params_init, **args)
            self.m_cov = tmp_cov
        elif opt_method=='Portmin':
            self.params = pm.minimize(lf, params_init, grad, self.ml_wishart_hessian)
        else:
            raise Exception("Unkown opt method {}.".format(opt_method))

        return of(self.params), regu(self.params) if regu is not None else None

    def minimize_newton(self, f, grad, hess, x0, eps=1e-7):
        return self.bfgs_method(f, grad, x0)[0]
        

    def minimize_adam(self, grad, x0, step=1.0, beta1=0.9, beta2=0.999,
                       chunk_size=75, num_epochs=30):
        def iteration(j, m_t, v_t, b1_t, b2_t, x):
            self.m_cov = np.cov(self.m_profiles[j:j+chunk_size, :],
                                    rowvar=False, bias=True)
            g = grad(x)
            m_t = beta1 * m_t + (1 - beta1) * g
            v_t = beta2 * v_t + (1 - beta1) * (g ** 2)
            b1_t *= beta1
            b2_t *= beta2
            m_t_hat, v_t_hat = m_t / (1 - b1_t), v_t / (1 - b2_t)
            x = x - step / (np.sqrt(v_t_hat) + 1e-8) * m_t_hat
            if np.isnan(self.ml_wishart(x)):
                return None
            return m_t, v_t, b1_t, b2_t, x
            
        x = x0.copy()
        n = len(x)
        m_t, v_t, b1_t, b2_t = np.zeros(n), np.zeros(n), 1, 1
        for i in range(num_epochs):
            np.random.shuffle(self.m_profiles)
            for j in range(0, len(self.m_profiles), chunk_size):
                ret = iteration(j, m_t, v_t, b1_t, b2_t, x)
                if ret is not None:
                    m_t, v_t, b1_t, b2_t, x = ret
        return x

    def minimize_momentum(self, grad, x0, step=0.002, resistance=0.9,
                          num_epochs=100, chunk_size=25):
        x = x0.copy()
        v_t = np.zeros(len(x))
        for i in range(num_epochs):
            np.random.shuffle(self.m_profiles)
            for j in range(0, len(self.m_profiles), chunk_size):
                self.m_cov = np.cov(self.m_profiles[j:j+chunk_size, :],
                                    rowvar=False, bias=True)
                v_t = resistance * v_t + step * grad(x)
                x -= v_t                
        return x
        
    def minimize_nesterov(self, grad, x0, step=0.002, resistance=0.9,
                          num_epochs=30, chunk_size=75):
        x = x0.copy()
        v_t = np.zeros(len(x))
        for i in range(num_epochs):
            np.random.shuffle(self.m_profiles)
            for j in range(0, len(self.m_profiles), chunk_size):
                self.m_cov = np.cov(self.m_profiles[j:j+chunk_size, :],
                                    rowvar=False, bias=True)
                v_t = resistance * v_t + step * grad(x - resistance * v_t)
                x -= v_t                
        return x        

    def unweighted_least_squares(self, params):
        m_sigma = self.calculate_sigma(params)
        m_cov = self.m_cov
        t = m_sigma - m_cov
        loss = np.trace(np.matmul(t, t.T))
        print(loss)
        return loss

    def naive_loss(self, params):
        m_sigma = self.calculate_sigma(params)
        m_cov = self.m_cov
        t = m_cov - m_sigma
        return np.linalg.norm(t)

    def general_least_squares(self, params):
        m_sigma = self.calculate_sigma(params)
        m_cov = self.m_cov
        w = np.linalg.inv(m_cov)
        t = (m_cov - m_sigma) @ w
        loss = np.trace(np.matmul(t, t.T))
        return loss

    def weighted_least_squares(self, params, weights):
        m_sigma = self.calculate_sigma(params)
        m_cov = self.m_cov
        t = m_sigma - m_cov
        w = np.linalg.inv(weights.T * weights)
        return np.trace(np.matmul(np.matmul(t, w), t.T))


    def ml_normal(self, params, alpha=0.01):
        """
        Multivariate Normal Distribution
        :param params:
        :param alpha:
        :return:
        """

        m_sigma = self.calculate_sigma(params)
        m_cov = self.m_cov

        # TODO need to be removed: A kind of regularisation
        if self.constraint_sigma(params) < 0:
            return 10 ** 20

        m_profiles = self.m_profiles
        log_likelihood_sigma = self.ml_norm_log_likelihood(m_sigma, m_profiles)
        log_likelihood_cov = self.ml_norm_log_likelihood(m_cov, m_profiles)
        loss = - (log_likelihood_sigma - log_likelihood_cov)

        # TODO: Strange moment
        if loss < 0:
            return self.min_loss


        # Remember the best loss_func value
        if (loss < self.min_loss) and (loss > 0):
            self.min_loss = loss
            self.min_params = params

        return loss


