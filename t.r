library(lavaan)
output_dir <- 'testingData/out_LAVAAN'
models_dir <- 'testingData/models'
i = 82
filename_mod <- paste0(models_dir, '/mod', i, '.txt')
filename_data <- paste0(models_dir, '/data', i, '.txt')

mod <- readLines(filename_mod)
data = read.csv(filename_data, header = TRUE)
fit <- sem(mod, data=data)
summary(fit)