import numpy as np
from time import time
import scipy.linalg.lapack as lapack

def inverse(x: np.array):
    #return np.linalg.inv(x)
    c, info = lapack.dpotrf(x)
    if info:
        raise np.linalg.LinAlgError
    lapack.dpotri(c, overwrite_c=1)
    c[np.tril_indices_from(c)] = c[np.triu_indices_from(c)]
    return c

def inverse2(x: np.array):
    #return np.linalg.inv(x)
    c, info = lapack.spotrf(x)
    if info:
        raise np.linalg.LinAlgError
    lapack.spotri(c, overwrite_c=1)
    c[np.tril_indices_from(c)] = c[np.triu_indices_from(c)]
    return c


n = 30
k = 1000
m = np.random.rand(n, n)
m = m @ m.T
m2 = np.random.rand(n, n, dtype='float32')

t = time()
for i in range(k):
    inverse(m)
print("Chol: ", (time() - t) / k)

t = time()
for i in range(k):
    inverse2(m)
print("Chol2: ", (time() - t) / k)

t = time()
for i in range(k):
    np.linalg.inv(m)
print("Inv: ", (time() - t) / k)